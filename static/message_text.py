message_text = {
    'start': 'Добрый день.',
    'start2': 'Вы обратились в бот команды юристов Банка, пожалуйста выберите один из варинтов',
    'agreement': 'Пожалуйста опишите для чего нужен договор',
    'agreement_doc': 'Пожалуйста приложите сканированную версию документов Партнера (иктибос, свидетельство и тд)',
    'edit_agreement_comment': 'Укажите пунк и новые условия',
    'edit_agreement_doc': 'Пожалуйста приложите документ  в формате Word для редактирования',
    'finish_send': 'Мы с вами свяжемся!',
    'check_doc': 'Пожалуйста опишите для чего нужен документ и кто его подготовил/предоставил',
    'document_comment': 'Пожалуйста приложите документ в формате Word',
    'ask': 'Пожалуйста опишите для чего нужен документ',
    'ask2': 'Пожалуйста опишите ситуацию, есть ли какие либо документы с которыми нужно ознакомиться. Если документ '
            'есть приложите документ в формате PDF файла',
    'ask_comment': 'Если это обращение внешнего Партнера приложите его обращение в формате PDF файла',
    'ask_comment2': 'Пожалуйста опишите ситуацию, есть ли какие либо документы с которыми нужно ознакомиться. Если '
                    'документ есть приложите документ в формате PDF файла',
    'ask_document': 'Прикрепить файл в формате PDF',
    'error_len': 'Минимальная количество символ не меньше 15 символов',
}
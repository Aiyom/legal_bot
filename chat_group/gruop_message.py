import json

from settings.conf import GROUP_ID
from static.message_text import message_text


async def get_user_id(unicode):
    with open('static/chats.json', 'r') as file:
        chats = file.read()
        chats = json.loads(chats)
        return chats[f"{unicode}"]


async def get_all_users():
    with open('static/chats.json', 'r') as file:
        chats = file.read()
        chats = json.loads(chats)
        return chats


async def write_user_id(chats):
    with open('static/chats.json', 'w') as file:
        chats = json.dumps(chats)
        file.write(chats)


async def send_msg(message):
    await message.forward(GROUP_ID)
    chats = await get_all_users()
    chats[f"{message['text']}"] = message['from']['id']
    await write_user_id(chats)


async def send_document(message):
    chats = await get_all_users()
    chats[f"{message['document']['file_unique_id']}"] = message['from']['id']
    await write_user_id(chats)


async def check_message_to_len(message):
    if len(message['text']) > 15:
        return True
    else:
        await message.answer(message_text['error_len'])
        return False


async def forward_document(message, state, state_text):
    dogovor_text = await state.get_data()
    await send_msg(dogovor_text[state_text])
    await message.forward(GROUP_ID)
    await send_document(message)
    await message.answer(message_text['finish_send'])
    await state.finish()


async def forward_text(message, state, state_text):
    if await check_message_to_len(message):
        await message.answer(message_text[state_text])
        async with state.proxy() as data:
            data[state_text] = message
            return True
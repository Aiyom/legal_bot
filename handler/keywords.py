from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton


async def start_button():
    button_text = KeyboardButton('Главная')
    button_cancel = KeyboardButton('Отменить действия')

    greet_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    greet_kb.add(button_text)
    greet_kb.add(button_cancel)
    return greet_kb


async def start():
    inline_kb_full = InlineKeyboardMarkup(row_width=2)
    inline_kb_full.add(InlineKeyboardButton('Подготовить договор', callback_data='agreement'))
    inline_kb_full.add(InlineKeyboardButton('Изменить/Дополнить договор', callback_data='edit_agreement'))
    inline_kb_full.add(InlineKeyboardButton('Проверить документ', callback_data='check_doc'))
    inline_kb_full.add(InlineKeyboardButton('Письменное обращение', callback_data='ask1'))
    inline_kb_full.add(InlineKeyboardButton('Получть ответ на вопрос', callback_data='ask2'))
    return inline_kb_full


async def send_comment_ask():
    inline_kb_full = InlineKeyboardMarkup(row_width=2)
    inline_kb_full.add(InlineKeyboardButton('Прикрепить файл', callback_data='file'))
    inline_kb_full.add(InlineKeyboardButton('Отправить письмо', callback_data='send'))
    return inline_kb_full
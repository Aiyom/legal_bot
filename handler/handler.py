from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text

from chat_group.gruop_message import *
from .keywords import *
from settings.conf import dp
from static.message_text import message_text
from state.states import LegalState


@dp.message_handler(commands="cancel", state="*")
@dp.message_handler(Text(equals="отмена", ignore_case=True), state="*")
async def satate_cancel(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("Действие отменено", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    text = message_text['start']
    await message.answer(text, reply_markup=await start_button())
    text = message_text['start2']
    await message.answer(text, reply_markup=await start())


@dp.message_handler(Text(equals="Отменить действия", ignore_case=True), state="*")
async def satate_cancel(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("Действие отменено")


@dp.message_handler(text=['Главная'])
async def send_welcome(message: types.Message):
    text = f"{message_text['start2']}"
    await message.answer(text, reply_markup=await start())


@dp.callback_query_handler(lambda c: c.data.startswith('check_doc'), state='*')
async def send_welcome(callback_query: types.CallbackQuery):
    text = message_text['check_doc']
    await callback_query.message.answer(text)
    await LegalState.document_comment.set()


@dp.callback_query_handler(lambda c: c.data.startswith('edit_agreement'), state='*')
async def send_welcome(callback_query: types.CallbackQuery):
    text = message_text['edit_agreement_comment']
    await callback_query.message.answer(text)
    await LegalState.edit_agreement_comment.set()


@dp.callback_query_handler(lambda c: c.data.startswith('agreement'), state='*')
async def send_welcome(callback_query: types.CallbackQuery):
    text = message_text['agreement']
    await callback_query.message.answer(text)
    await LegalState.dogovor.set()


@dp.callback_query_handler(lambda c: c.data.startswith('ask'), state='*')
async def send_welcome(callback_query: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['ask'] = callback_query['data']
    if callback_query['data'] == 'ask1':
        text = message_text['ask']
        await callback_query.message.answer(text)
        await LegalState.check_letter_comment.set()
    elif callback_query['data'] == 'ask2':
        text = message_text['ask2']
        await callback_query.message.answer(text)
        await LegalState.check_letter_comment.set()


@dp.callback_query_handler(lambda c: c.data.startswith('send'), state='*')
async def send_welcome(callback_query: types.CallbackQuery, state: FSMContext):
    ask_comment = await state.get_data()
    await send_msg(ask_comment['ask_comment'])
    await callback_query.message.answer(message_text['finish_send'])
    await callback_query.message.edit_reply_markup(reply_markup=None)
    await state.finish()


@dp.callback_query_handler(lambda c: c.data.startswith('file'), state='*')
async def send_welcome(callback_query: types.CallbackQuery):
    text = message_text['ask_document']
    await callback_query.message.answer(text)
    await callback_query.message.edit_reply_markup(reply_markup=None)
    await LegalState.check_letter.set()

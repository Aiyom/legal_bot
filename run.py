from aiogram.utils import executor
# from settings.conf import dp
from handler.handler import *
from state.state_handler import *

if '__main__' == __name__:
    executor.start_polling(dp, skip_updates=True)
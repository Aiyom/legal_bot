from aiogram.dispatcher.filters.state import StatesGroup, State


class LegalState(StatesGroup):
    document = State()
    document_comment = State()
    dogovor = State()
    dogovor_doc = State()
    check_letter_comment = State()
    check_letter = State()
    ask_document = State()
    edit_agreement_comment = State()
    edit_agreement_document = State()
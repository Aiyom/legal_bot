from aiogram import types
from aiogram.dispatcher import FSMContext

from handler.keywords import send_comment_ask
from static.message_text import message_text
from settings.conf import dp
from chat_group.gruop_message import get_user_id, send_msg, check_message_to_len, forward_document, \
    forward_text
from state.states import LegalState


@dp.message_handler(state=LegalState.document_comment, content_types=['text'])
async def process_message(message: types.Message, state: FSMContext):
    is_True = await forward_text(message, state, 'document_comment')
    if is_True:
        await LegalState.document.set()


@dp.message_handler(state=LegalState.document, content_types=['document'])
async def process_message(message: types.Message, state: FSMContext):
    await forward_document(message, state, 'document_comment')


@dp.message_handler(state=LegalState.edit_agreement_comment, content_types=['text'])
async def process_message(message: types.Message, state: FSMContext):
    is_True = await forward_text(message, state, 'edit_agreement_doc')
    if is_True:
        await LegalState.edit_agreement_document.set()


@dp.message_handler(state=LegalState.edit_agreement_document, content_types=['document'])
async def process_message(message: types.Message, state: FSMContext):
    await forward_document(message, state, 'edit_agreement_doc')


@dp.message_handler(state=LegalState.dogovor, content_types=['text'])
async def process_message(message: types.Message, state: FSMContext):
    is_True = await forward_text(message, state, 'edit_agreement_doc')
    if is_True:
        await LegalState.dogovor_doc.set()


@dp.message_handler(state=LegalState.dogovor_doc, content_types=['document'])
async def process_message(message: types.Message, state: FSMContext):
    await forward_document(message, state, 'edit_agreement_doc')


@dp.message_handler(state=LegalState.check_letter_comment, content_types=['text'])
async def process_message(message: types.Message, state: FSMContext):
    if await check_message_to_len(message):
        asc_comment = await state.get_data()
        if asc_comment['ask'] == 'ask1':
            await message.answer(message_text['ask_comment'], reply_markup=await send_comment_ask())
        elif asc_comment['ask'] == 'ask2':
            await message.answer(message_text['ask_comment2'], reply_markup=await send_comment_ask())
        async with state.proxy() as data:
            data['ask_comment'] = message


@dp.message_handler(state=LegalState.check_letter, content_types=['document'])
async def process_message(message: types.Message, state: FSMContext):
    await forward_document(message, state, 'ask_comment')


@dp.message_handler(content_types=["document"])
async def get_updates(message: types.Message):
    if message['chat']['type'] == 'supergroup' and 'reply_to_message' in message:
        try:
            send_to = await get_user_id(f"{message['reply_to_message']['document']['file_unique_id']}")
            if send_to is None:
                send_to = await get_user_id(f"{message['reply_to_message']['text']}")
            await message.send_copy(send_to)
        except Exception:
            pass


@dp.edited_message_handler(content_types=["text"])
async def get_updates(message: types.Message):
    await send_msg(message)


@dp.message_handler(content_types=["text"])
async def get_updates(message: types.Message):
    if 'reply_to_message' in message and message['chat']['type'] == 'supergroup':
        if 'document' in message['reply_to_message']:
            try:
                send_to = await get_user_id(f"{message['reply_to_message']['document']['file_unique_id']}")
                await message.send_copy(send_to)
            except Exception:
                pass
        else:
            try:
                send_to = await get_user_id(f"{message['reply_to_message']['text']}")
                await message.send_copy(send_to)
            except Exception:
                pass
    elif message['chat']['type'] == 'private' and await check_message_to_len(message):
        await send_msg(message)
